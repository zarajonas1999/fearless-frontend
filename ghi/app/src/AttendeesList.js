import App from './App';




function AttendeesList(props){
    return(
        <table className='table'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
          {props.attendees.map(attendee => {
            return (
              <tr key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

// key property is added into the <tr> tag to specify the uniqueness of each attendee using the attendee object's href
export default AttendeesList;
