import React, {useEffect, useState} from 'react';




function PresentationForm (props) {

    const [presenterName,setPresenterName] = useState('')
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const[presenterEmail,setPresenterEmail] = useState('');
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const[companyName,setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const[title,setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const[synopsis,setSynopsis] = useState('');
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const[conference,setConference] = useState('');
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        console.log(data);

        const locationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            const newPresentation = await response.json();
            console.log(newPresentation);

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');

        }
    }

    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form
            id="create-location-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handlePresenterNameChange}
                value={presenterName}
                placeholder="Name"
                required type="text"
                name="presenter_name"
                id="presenter_name"
                className="form-control"
                />
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handlePresenterEmailChange}
                value={presenterEmail}
                placeholder="Room count"
                required type="text"
                name="room_count"
                id="room_count"
                className="form-control"
                />
                <label htmlFor="room_count">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleCompanyNameChange}
                value={companyName}
                placeholder="Company Name"
                required type="text"
                name="company_name"
                id="company_name"
                className="form-control"
                />
                <label htmlFor="city">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleTitleChange}
                value={title}
                placeholder="Title"
                required type="text"
                name="title"
                id="title"
                className="form-control"
                />
                <label htmlFor="city">Title</label>
              </div>
              <div className="mb-3">
              <label htmlFor="description">Synopsis</label>
              <textarea onChange={handleSynopsisChange}
              value = {synopsis}
              id="synopsis"
              rows="3"
              name="synopsis"
              className="form-control"
              >
              </textarea>
              </div>
              <div className="mb-3">
                <select
                onChange={handleConferenceChange}
                value={conference}
                required name="conference"
                id="conference"
                className="form-select"
                >
                  <option  value="">Choose a conference</option>
                  {conferences.map(x => {
                    return (
                        <option key = {x.id} value = {x.id}>
                            {x.name}
                        </option>
                    )
                  }) }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default PresentationForm;
