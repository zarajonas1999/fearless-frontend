window.addEventListener('DOMContentLoaded', async () => {
    const conferencesUrl = 'http://localhost:8000/api/conferences/'
    const locationsUrl = 'http://localhost:8000/api/locations/'
    const locationsUrlResponse = await fetch(locationsUrl)
    if (!locationsUrlResponse.ok){
        throw new Error(`Uh oh, Status: ${conferencesUrlResponse.status}`);
    }
    const locationsData = await locationsUrlResponse.json();
    const locationsDataArray = locationsData.locations
    // console.log(locationsData)


    const selectTag = document.getElementById('location'); //gets tags in html doc with id = state
    // console.log(selectTag)
    for (let locations of locationsDataArray){
        const option = document.createElement('option'); //creates option tag for dropdown
        option.value = locations.id
        option.innerHTML = locations.name
        selectTag.appendChild(option)
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(conferencesUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);

        }

    })

});
