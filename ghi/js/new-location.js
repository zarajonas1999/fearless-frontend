window.addEventListener('DOMContentLoaded', async () => {
    const statesUrl = 'http://localhost:8000/api/states/'
    const statesUrlResponse = await fetch(statesUrl)
    if (!statesUrlResponse.ok){
        throw new Error(`Uh oh, Status: ${statesUrlResonse.status}`);
    }
    const stateData = await statesUrlResponse.json();
    // console.log(stateData)

    const selectTag = document.getElementById('state'); //gets tags in html doc with id = state
    for (let state of stateData.states){
        const option = document.createElement('option');
        option.value = state.abbreviation
        option.innerHTML = state.name
        selectTag.appendChild(option)
        // console.log(selectTag);
        // console.log(option.value);
        // console.log(state)

    }
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);

        }
        // console.log(json);
    })

});
