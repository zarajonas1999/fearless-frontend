function createCard(name, description, picture) {
    return `
      <div class="card">
        <img src="${picture}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    let boom = document.querySelectorAll('.col')
    try {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      console.log(response);

      const data = await response.json();
      const conferenceRow = document.querySelector('.row'); //row is the container for columns
      const maxColumns = 3;
      let columnIndex = 0; //use let so it can dynamically change

      for (let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl)
        if (detailResponse.ok){
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details['conference']['description'];
            const picture = details['conference']['location']['picture_url'];
            const html = createCard(name,description,picture);
            const rowChild = document.createElement('div') //creates div tag child in row
            if (conferenceRow.children.length === 3) {rowChild.classList.add('card')}
            else {rowChild.classList.add('col')}; //adds col class to the div created prior
            rowChild.innerHTML = html // adds the interpolation from the createCard function
            if (conferenceRow.children.length === 3 && columnIndex !== maxColumns ){
                boom[columnIndex].appendChild(rowChild);
                columnIndex += 1
            }
            else if (columnIndex !== maxColumns){
                conferenceRow.appendChild(rowChild);
                columnIndex += 1
            } else{
                console.log(boom);
                columnIndex = 0
                boom = document.querySelectorAll('.col')
                boom[columnIndex].appendChild(rowChild)
                columnIndex += 1

            }


            // if columnIndex reaches 0, the next append should be done to the div col
            // console.log(details);
            // console.log(html);
            // console.log(conference);
      }

      }
    }
    catch (error) {
      console.error('An error occurred:', error.message);
    }
  });
